package com.citi.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class A {
	
	@Autowired
	private B bObject;
	
	@Autowired
	private C cObject;
	
	public B getbObject() {
		return bObject;
	}
	public void setbObject(B bObject) {
		this.bObject = bObject;
	}
	public C getcObject() {
		return cObject;
	}
	public void setcObject(C cObject) {
		this.cObject = cObject;
	}

}
