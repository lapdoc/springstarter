package com.citi.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan
@PropertySource("classpath:application.properties")
public class Application {
		
	public static void main(String[] args) {
		
		try (AnnotationConfigApplicationContext context = 
				new AnnotationConfigApplicationContext(Application.class)) {
			
			A aObject = context.getBean(A.class); 
//			A aObject = new A();
//			B bObject = new B();
//			C cObject = new C();
//			
//			aObject.setbObject(bObject);
//			aObject.setcObject(cObject);
//			bObject.setcObject(cObject);
//			cObject.setMode("NORMAL");
			
			System.out.println(aObject.getbObject().getcObject().getMode());
		}
		
	}
}
