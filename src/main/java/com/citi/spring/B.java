package com.citi.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class B {
	
	@Autowired
	private C cObject;
	
	public C getcObject() {
		return cObject;
	}
	public void setcObject(C cObject) {
		this.cObject = cObject;
	}
}
